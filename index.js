var random = new Vue({
    el: "#tema",
    data: {
        shapes: ['purple', 'blue', 'green', 'yellow', 'black', 'pink'],
    },
    methods: {
        click: function () {
            const array = []
            this.shapes.forEach(() => {
                array.push('#' + parseInt(Math.random() * 0xffffff).toString(16))

            })
            this.shapes = [...array]
        }
    }
})